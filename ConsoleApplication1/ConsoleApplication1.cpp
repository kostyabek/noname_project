﻿#include <iostream>
#include "windows.h"
#define _USE_MATH_DEFINES
#include "math.h"
#include "iomanip"

using namespace std;

int main()
{
	int n, sum = 0;
	float x;
	double y;
	cout << "Enter x: ";
	cin >> x;
	for (n = 1; n < 11; n++) {
		sum = sum + n * n;
	}
	y = (sum + sin(x * M_PI / 180)) / (x + 2);
	cout << "Result = " << setw(5) << setprecision(5) << y << endl;
	system("pause");
	return 0;
}