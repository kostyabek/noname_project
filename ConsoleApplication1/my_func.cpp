#define _USE_MATH_DEFINES
#include <math.h>
#include <stdio.h>
double myFunc1(float x, float y, float z) {
	if (x == -y)
		printf("Error: x == -y!\n");
	else
		return pow(x, 3) * pow(tan(pow(((x + y) * M_PI / 180), 2)), 2) + z / (sqrt(x + y)) - 2 * tan(25 * M_PI / 180);
}
double myFunc2(float x, float y, float z) {
	if ((x == 0) || (y == 0) || (x == y))
		printf("Error!\nStick to the restrictions: x != 0, y != 0, x != y\n");
	else
		return ((y*pow(x, 2) - z) / (pow(M_E, (y*x)) - 1)) + ((pow((5-pow(y, 3)), 1/3.)) / (fabs(x-y)) / (sqrt(2*x)));
}
double myFunc3(int a, float b) {
	return (a + b) / 2;
}